package org.lucky.demo;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import static org.apache.spark.sql.functions.*;
import org.lucky.demo.config.SpConfig;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.apache.log4j.Logger;
import org.apache.log4j.Level;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) throws Exception {
		Logger.getLogger("org").setLevel(Level.OFF);
		Logger.getLogger("akka").setLevel(Level.OFF);
		@SuppressWarnings("resource")
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpConfig.class);
		SparkSession sparkSession = context.getBean("sparkSession", SparkSession.class);

		Dataset<Row> dataset = sparkSession.read()
				.option("header", true)
				.option("delimiter", ";")
				.csv("D:/Learning/ML_DATA/bank/bank.csv");

		dataset.printSchema();
		dataset=dataset.where(col("y").equalTo("yes"))
				       .withColumn("job", 
						when(col("job").equalTo("student"), 1)
						.when(col("job").equalTo("retired"), 2)
						.when(col("job").equalTo("management"), 3)
						.when(col("job").equalTo("technician"), 4)
						.when(col("job").equalTo("blue-collar"), 5)
						.when(col("job").equalTo("admin."), 6)
						.when(col("job").equalTo("services"), 7)
						.when(col("job").equalTo("unemployed"), 0)
						.otherwise(col("job")));
		
		
		
				dataset.show();
	}
}
