package org.lucky.demo.ch4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import org.lucky.demo.config.SpConfig;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Service;

import scala.Tuple2;

@Service
public class PairedRDDDemo1 {

	
	

	public static void main(String[] args) {
		Logger.getLogger("org").setLevel(Level.OFF);
		Logger.getLogger("akka").setLevel(Level.OFF);
		@SuppressWarnings("resource")
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpConfig.class);
		
		JavaSparkContext jsc=context.getBean("javaSparkContext", JavaSparkContext.class);
		
		java.util.List<Tuple2<String, String>> list = new java.util.ArrayList<Tuple2<String, String>>();
		list.add(new Tuple2<String, String>("1", "lucky"));
		list.add(new Tuple2<String, String>("2", "praful"));
		list.add(new Tuple2<String, String>("3", "tushar"));
		list.add(new Tuple2<String, String>("4", "priti"));
		
		JavaPairRDD<String, String> parallelizePairs = jsc.parallelizePairs(list);
		List<Tuple2<String, String>> take = parallelizePairs.take(5);
		for(Tuple2<String, String> t : take){
			System.out.println(t._1+" : "+t._2);
		}
		
		List<String> lines=new ArrayList<String>();
		lines.add("hello how are you? ");
		lines.add("Iam fine. how are you? ");
		lines.add("sab badhiaya yu bata ");
		lines.add("sab thik tu bata ");
		lines.add("tu bata? ");
		
		PairFunction<String, String, String> keyData =new PairFunction<String, String, String>() { 
			private static final long serialVersionUID = -4820704173541384353L;
			public Tuple2<String, String> call(String x) throws Exception {	
				return new Tuple2<String, String>(x.split(" ")[0], x);
			}
		};
		
		JavaPairRDD<String, String> mapToPair = jsc.parallelize(lines).mapToPair(keyData);
		List<Tuple2<String, String>> take2 = mapToPair.take(5);
		for(Tuple2<String, String> t : take2){
			System.out.println(t._1+" : "+t._2);
		}
		
		List<Tuple2<String, String>> take3 = mapToPair.reduceByKey(

				//Using JAVA-7
				/*new Function2<String, String, String>() {
					private static final long serialVersionUID = 7924816121816257974L;
					public String call(String arg0, String arg1) throws Exception {
						return arg0+" : "+arg1;
					}
				}*/
				//Using JAVA-8
				(String x, String y) ->  x+" : "+y
				).take(5);
		System.out.println("\nList to PairRDD");
		for(Tuple2<String, String> t : take3){
			System.out.println(t._1+" : "+t._2);
		}
		
		// Group by key
		
		JavaPairRDD<String, Iterable<String>> groupByKey = mapToPair.groupByKey();
		System.out.println("\nGroup by key");
		for(Tuple2<String, Iterable<String>> t : groupByKey.take(5)){
			System.out.println(t._1+" : "+t._2.toString());
		}
		
		//Map values
		JavaPairRDD<String, String> mapValues = mapToPair.mapValues(f -> ""+f.length()/3);
		System.out.println("\nMapValues example");
		for(Tuple2<String, String> t : mapValues.take(5)){
			System.out.println(t._1+" : "+t._2);
		}
		
		//flatMapValues
		JavaPairRDD<String, String> flatMapValues = mapToPair.flatMapValues(
			//JAVA_7	
	/*	 new Function<String, Iterable<String>>() {
			private static final long serialVersionUID = 2524218193987388748L;

			@Override
			public Iterable<String> call(String arg0) throws Exception {
				
				return Arrays.asList(arg0.split(" "));
			}			
		}*/
		//JAVA_8
		(String x) -> Arrays.asList(x.split(" "))
				 
		);
		
		System.out.println("\nFlatMapValues example");
		for(Tuple2<String, String> t : flatMapValues.sortByKey().take(25)){
			System.out.println(t._1+" : "+t._2);
		}
	}

}
