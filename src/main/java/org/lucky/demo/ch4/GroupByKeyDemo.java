package org.lucky.demo.ch4;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.PairFunction;
import org.lucky.demo.config.SpConfig;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import scala.Tuple2;

public class GroupByKeyDemo {

	public static void main(String[] args) {
		Logger.getLogger("org").setLevel(Level.OFF);
		Logger.getLogger("akka").setLevel(Level.OFF);
		@SuppressWarnings("resource")
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpConfig.class);	
		JavaSparkContext jsc=context.getBean("javaSparkContext", JavaSparkContext.class);
		
		/*java.util.List<Tuple2<String, Integer>> list = new java.util.ArrayList<Tuple2<String, Integer>>();
		list.add(new Tuple2<String, Integer>("lucky", 10));
		list.add(new Tuple2<String, Integer>("praful", 30));
		list.add(new Tuple2<String, Integer>("lucky", 40));
		list.add(new Tuple2<String, Integer>("tushar", 60));
		list.add(new Tuple2<String, Integer>("Aniket", 70));
		list.add(new Tuple2<String, Integer>("Chitresh", 78));
		JavaPairRDD<String,Integer> pairs = jsc.parallelizePairs(list);
		
		System.out.println("********** Input Paires ***************");
		for(Tuple2<String, Integer> take : pairs.take(10))
			System.out.println(take.toString());
		
		System.out.println("********** Group by key O/P *****************");
		for(Tuple2<String, Iterable<Integer>> take : pairs.groupByKey().take(10))
			System.out.println(take.toString());*/
		
		JavaRDD<String> csv = jsc.textFile("./src/main/resources/movie_metadata.csv");
		JavaRDD<Tuple2<String, String>> mapRdd = csv.map((Function<String, Tuple2<String,String>>) (String f) -> {
			       return new Tuple2<String,String>(f.split(",")[1],f);
			        }
		);
		System.out.println();
		System.out.println("********** Movie dataset Map RDD **********");
		for(Tuple2<String, String> take : mapRdd.take(10))
			System.out.println(take.toString());
		
		JavaPairRDD<String, String> mapToPairRdd = mapRdd.mapToPair(new PairFunction<Tuple2<String,String>, String, String>() {
			private static final long serialVersionUID = 1L;
			@Override
			public Tuple2<String, String> call(Tuple2<String, String> t) throws Exception {
				return t;
			}
		});
		System.out.println();
		System.out.println("********** Movie dataset Map PAIRD RDD **********");
		for(Tuple2<String, String> take : mapToPairRdd.take(10))
			System.out.println(take.toString());
		
		JavaPairRDD<String, Iterable<String>> groupByKey = mapToPairRdd.groupByKey();
		System.out.println();
		System.out.println("********** Movie dataset Group by key RDD **********");
		for(Tuple2<String, Iterable<String>> take : groupByKey.take(10))
			System.out.println(take.toString());

	}

}
