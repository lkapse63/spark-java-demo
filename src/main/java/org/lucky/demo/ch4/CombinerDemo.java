package org.lucky.demo.ch4;

import java.io.Serializable;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.lucky.demo.config.SpConfig;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import scala.Tuple2;

public class CombinerDemo {

	public static void main(String[] args) {
		Logger.getLogger("org").setLevel(Level.OFF);
		Logger.getLogger("akka").setLevel(Level.OFF);
		@SuppressWarnings("resource")
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpConfig.class);	
		JavaSparkContext jsc=context.getBean("javaSparkContext", JavaSparkContext.class);
		
		java.util.List<Tuple2<String, Integer>> list = new java.util.ArrayList<Tuple2<String, Integer>>();
		list.add(new Tuple2<String, Integer>("lucky", 10));
		list.add(new Tuple2<String, Integer>("praful", 30));
		list.add(new Tuple2<String, Integer>("lucky", 40));
		list.add(new Tuple2<String, Integer>("tushar", 60));
		JavaPairRDD<String,Integer> pairs = jsc.parallelizePairs(list);
		
		System.out.println("Input Paires");
		for(Tuple2<String, Integer> take : pairs.take(10))
			System.out.println(take.toString());
		

		//Combiner code
		Function<Integer, AvgCount> createCombiner = new Function<Integer, AvgCount>() {
			private static final long serialVersionUID = -8490663349741749447L;
			@Override
			public AvgCount call(Integer key) throws Exception {
				return new AvgCount(key, 1);
			}
		};
		
		//Merger code
		Function2<AvgCount, Integer, AvgCount> mergeValue = new Function2<AvgCount, Integer, AvgCount>() {
			private static final long serialVersionUID = 1L;
			@Override
			public AvgCount call(AvgCount avg, Integer v) throws Exception {
				avg.total +=v;
				avg.num +=1;
				return avg;
			}
		};
		
		//Merge combiner code
		Function2<AvgCount, AvgCount, AvgCount> mergeCombiners = new Function2<AvgCount, AvgCount, AvgCount>() {
			private static final long serialVersionUID = 1L;
			@Override
			public AvgCount call(AvgCount avga, AvgCount avgb) throws Exception {
				avga.total +=avgb.total;
				avga.num=avgb.num;
				return avga;
			}
		};
		
		JavaPairRDD<String, AvgCount> combineByKey = pairs.combineByKey(createCombiner, mergeValue, mergeCombiners);
		
		System.out.println("Output of combine by key");
		for(Tuple2<String, AvgCount> take: combineByKey.take(10)){
//			System.out.println("key:: "+take._1+" Avg:: "+take._2.avg());
			System.out.println(take.toString());
		}
		
		
		jsc.stop();
	}
	
	static class AvgCount implements Serializable{

		private static final long serialVersionUID = -1525172506541704511L;
		
		int total;
		int num;
		
		public AvgCount(int total, int num){
			this.total=total;
			this.num=num;
		}
		
		public float avg(){
			return total/(float) num;
		}

		@Override
		public String toString() {
			return "AvgCount [total=" + total + ", num=" + num + "]";
		}
		
		
		
	}
	
	

}
