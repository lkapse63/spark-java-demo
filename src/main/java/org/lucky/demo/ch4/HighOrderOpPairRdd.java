package org.lucky.demo.ch4;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.Optional;
import org.lucky.demo.config.SpConfig;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import scala.Tuple2;

public class HighOrderOpPairRdd {

	public static void main(String[] args) {
		Logger.getLogger("org").setLevel(Level.OFF);
		Logger.getLogger("akka").setLevel(Level.OFF);
		@SuppressWarnings("resource")
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpConfig.class);		
		JavaSparkContext jsc=context.getBean("javaSparkContext", JavaSparkContext.class);
		
		
		List<String> lines=new ArrayList<String>();
		lines.add("hello how are you? ");
		lines.add("Iam fine. how are you? ");
		lines.add("sab badhiaya yu bata ");
		lines.add("sab thik tu bata ");
		lines.add("tu bata? ");
		
		JavaPairRDD<String, String> pairRddLeft = jsc.parallelize(lines).mapToPair(
				              line ->  new Tuple2<String, String>(line.split(" ")[0], line));
		//Adding more lines
		List<String> lines2=new ArrayList<String>();
		lines2.add("hello how are you? ");
		lines2.add("Iam fine. how are you? ");
		lines2.add("sab badhiaya yu bata ");
		lines2.add("sab thik tu bata ");
		lines2.add("tu bata? ");
		lines2.add("kuch bhi to nahi, tu bata");
		lines2.add("pahale jaisa hi, tu bata");
		JavaPairRDD<String, String> pairRddRight = jsc.parallelize(lines2).mapToPair(
	              line ->  new Tuple2<String, String>(line.split(" ")[0], line));

		// Subtracting RDD
		JavaPairRDD<String, String> subtract = pairRddRight.subtract(pairRddLeft);
		System.out.println("subtracting two RDD");
		for(Tuple2<String, String> take : subtract.take(10)){
			System.out.println(take._1+" : "+take._2);
		}
		
		//JOIN RDD
		System.out.println("\nJOIN two RDD");
		for(Tuple2<String, Tuple2<String, String>> take : pairRddLeft.join(pairRddRight).take(10)){
			System.out.println(take._1+" : "+take._2.toString());
		}
		
		System.out.println("\nRightOuterJoin two RDD");
		for(Tuple2<String, Tuple2<Optional<String>, String>> take : pairRddLeft.rightOuterJoin(pairRddRight).take(10)){
			System.out.println(take._1.toString()+" : "+take._2.toString());
		}
	}

}
