package org.lucky.demo.config;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.SparkSession;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages="org.lucky")
public class SpConfig {
	
	/**
	 * At a time ony one context
	 * @Bean(name="sparkSession")
	public SparkSession getSparkSession(){
		SparkConf conf = getSparkConf();
		SparkSession session = SparkSession
				                 .builder()
				                 .config(conf)
				                 .getOrCreate();
		return session;
		
	}*/

	public SparkConf getSparkConf() {
		SparkConf conf = new SparkConf();
		conf.setMaster("local")
		    .setAppName("spark_demo");
		return conf;
	}
	
	@Bean(name="javaSparkContext")
	public JavaSparkContext javaSparkContext(){
		    JavaSparkContext jsc = new JavaSparkContext(getSparkConf());
		    return jsc;
	}

}
